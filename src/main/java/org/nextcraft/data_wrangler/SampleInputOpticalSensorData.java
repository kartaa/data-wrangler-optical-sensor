package org.nextcraft.data_wrangler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.metamodel.DataContext;
import org.apache.metamodel.DataContextFactory;
import org.apache.metamodel.UpdateCallback;
import org.apache.metamodel.UpdateScript;
import org.apache.metamodel.UpdateableDataContext;
import org.apache.metamodel.csv.CsvConfiguration;
import org.apache.metamodel.data.DataSet;
import org.apache.metamodel.data.Row;
import org.apache.metamodel.query.Query;
import org.apache.metamodel.schema.ColumnType;
import org.apache.metamodel.schema.Schema;
import org.apache.metamodel.schema.Table;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

/**
 * Sample the sensor data file
 * 
 * @author ashishar
 *
 */
public class SampleInputOpticalSensorData {

	public void generateSamples(File input, File output, SamplingType type) throws IOException {
		System.out.println("Input: " + input.getAbsolutePath());
		System.out.println("Output: " + output.getAbsolutePath());
		System.out.println("Type: " + type.name());

		// Read from input
		CsvConfiguration conf = new CsvConfiguration(CsvConfiguration.NO_COLUMN_NAME_LINE, "UTF-8", '\t', '"', '\\');
		DataContext dataContext = DataContextFactory.createCsvDataContext(input, conf);
		Schema schema = dataContext.getDefaultSchema();
		Table[] tables = schema.getTables();

		// CSV files only has a single table in the default schema
		assert tables.length == 1;

		Table table = tables[0];
		Query query = dataContext.query().from(table).select(table.getColumns()).toQuery();

		/*
		 * 0 - timestamp 1, 2, 3, 4, 5 - sensor values
		 */
		int TIMESTAMP_COLUMN_INDEX = 0, COL2_COLUMN_INDEX = 1, COL3_COLUMN_INDEX = 2;
		DataSet dataSet = dataContext.executeQuery(query);

		Map<String, List<Row>> samplesBySeconds = new HashMap<>(60000);
		List<String> keySequence = new ArrayList<>();

		// Read All Samples
		while (dataSet.next()) {
			Row r = dataSet.getRow();
			String timestamp = r.getValue(TIMESTAMP_COLUMN_INDEX).toString();
			String[] parts = timestamp.split("\\.");
			if (parts.length < 2) {
				System.err.println("Skipping Row, " + r);
				continue;
			}
			String key = parts[0];

			if (!samplesBySeconds.containsKey(key)) {
				keySequence.add(key);
				samplesBySeconds.put(key, new ArrayList<>());
			}

			samplesBySeconds.get(key).add(r);
		}
		// Select samples in key order
		List<Row> selection = new ArrayList<>();
		for (String key : keySequence) {
			selection.add(selectRow(samplesBySeconds.get(key), type));

		}

		// Write samples to new file
		if (output.exists()) {
			output.delete();
		}
		UpdateableDataContext wdataContext = DataContextFactory.createCsvDataContext(output, conf);
		final Schema wschema = wdataContext.getDefaultSchema();
		wdataContext.executeUpdate(new UpdateScript() {
			public void run(UpdateCallback callback) {

				// CREATING A TABLE
				Table table = callback.createTable(schema, "my_table").withColumn("A").ofType(ColumnType.CHAR)
						.withColumn("B").ofType(ColumnType.CHAR).withColumn("C").ofType(ColumnType.CHAR).withColumn("D")
						.ofType(ColumnType.CHAR).withColumn("E").ofType(ColumnType.CHAR).withColumn("F")
						.ofType(ColumnType.CHAR).execute();

				// INSERTING ROWS
				for (Row r : selection) {
					callback.insertInto(table).value("A", r.getValue(0)).value("B", r.getValue(1))
							.value("C", r.getValue(2)).value("D", r.getValue(3)).value("E", r.getValue(4))
							.value("F", r.getValue(5)).execute();
				}

			}
		});
	}

	private Row selectRow(List<Row> value, SamplingType type) {

		if (value.size() < 1) {
			return null;
		}
		Row s = null;

		switch (type) {
		case LAST:
			s = value.get(value.size() - 1);
			break;
		case RANDOM:
			s = value.get(ThreadLocalRandom.current().nextInt(0, value.size()));
			break;
		case FIRST:
		default:
			s = value.get(0);
		}
		return s;
	}

	public static void main(String[] argv) {
		SampleInputOpticalSensorData s = new SampleInputOpticalSensorData();

		Args args = s.new Args();
		JCommander j = JCommander.newBuilder().addObject(args).build();
		j.parse(argv);
		// Show usage information
		if (args.usage == true) {
			j.usage();
		} else {
			SamplingType type = s.determineType(args.type);
			File input = args.file, output = null;
			if (input != null && input.exists()) {
				output = new File(input.getName() + ".out");
				try {
					s.generateSamples(input, output, type);
					if (args.fixquote) {
						s.generateFixedOutput(output);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void generateFixedOutput(File output) throws IOException {
		// TODO Auto-generated method stub
		String s;
		String search = "\"";
		String replacement = "";
		FileReader fr = new FileReader(output);
		BufferedReader br = new BufferedReader(fr);

		File fixedOutput = new File(output.getName() + ".fixed");
		FileWriter fw = new FileWriter(fixedOutput);
		BufferedWriter bw = new BufferedWriter(fw);

		while ((s = br.readLine()) != null) {
			bw.write(s.replaceAll(search, replacement));
			bw.write(System.lineSeparator());
		}
		bw.close();
		fw.close();
		br.close();
		fr.close();
	}

	private SamplingType determineType(String type) {
		SamplingType _type = null;
		if (type == null) {
			return SamplingType.FIRST;
		}
		switch (type) {
		case "first":
			_type = SamplingType.FIRST;
			break;
		case "last":
			_type = SamplingType.LAST;
			break;
		case "maxcol2":
			_type = SamplingType.MAXCOL2;
			break;
		case "maxcol3":
			_type = SamplingType.MAXCOL3;
			break;
		case "random":
			_type = SamplingType.RANDOM;
			break;
		default:
			_type = SamplingType.FIRST;
		}

		return _type;
	}

	/**
	 * Protected classes.
	 * 
	 * @author ashishar
	 *
	 */
	protected class Args {
		@Parameter(names = "-samplingtype", required = true, description = "Choose a sampling style as one from [first, last, maxcol2, maxcol3, random]")
		private String type;

		@Parameter(names = "-fixquote", description = "Fix quote characters")
		private boolean fixquote = false;

		@Parameter(names = "-debug", description = "Debug mode")
		private boolean debug = false;

		@Parameter(names = "-usage", description = "Usage")
		private boolean usage = false;

		@Parameter(names = "-file", required = true, converter = FileConverter.class)
		private File file;
	}

	protected class FileConverter implements IStringConverter<File> {
		@Override
		public File convert(String value) {
			return new File(value);
		}
	}

	protected enum SamplingType {
		FIRST, LAST, MAXCOL2, MAXCOL3, RANDOM
	}
}
